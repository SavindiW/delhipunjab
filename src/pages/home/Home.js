import React,{useState,useEffect} from "react";
import { Layout } from 'antd';
import Header from "../../components/header/Header";
import Footer from "../../components/footer/Footer";
import Card from "../../components/card/PostCard";
import "./HomeStyles.css";
import Masonry, {ResponsiveMasonry} from "react-responsive-masonry"
import { Spin, Space } from 'antd';
import {loadAllPosts} from '../../actions/auth';
import {connect} from 'react-redux';

export const Home = ({post,allposts,loadAllPosts}) => {
    const [posts,setPosts] = useState([])
    const { Content } = Layout;
    const [loading,setloading]=useState(true);
 

    useEffect(()=>{
        console.log("i am running")
        loadAllPosts();
        setTimeout(()=>{
            setPosts(allposts);
            if (typeof(allposts)==="object" ){
            
                setloading(false)
            }
        },1000)
        
        
      
    },[typeof(allposts),post,loading])


    if (!loading){
        return ( 
            <Layout className="layout">
                <Header/>
                <Content className="site-layout" >
                    <div className='home__post_container'>
                        <ResponsiveMasonry
                            columnsCountBreakPoints={{350: 1, 500: 2, 800:3, 1000: 4}}
                        >
                            <Masonry>
                                {posts.map((post,i)=>(
                                    <Card 
                                        key={i}
                                        id = {post.id}
                                        imgURL = {post.imgURL}
                                        title={post.title}
                                        description = {post.description}
                                        author = {post.author}
                                        created = {post.created}
                                        likes = {post.likes}
                                        views = {post.views}
                                        comment_count = {post.comment_count}
                                    />    
                                ))}
                            </Masonry>
                        </ResponsiveMasonry>
                    </div>
                </Content>
                <Footer/>        
            </Layout> 
    )
    }
    else{
        return(
            <div className='home__loading'>
                <Space size="middle">
                    <Spin size="large" />
                </Space>
            </div>   
        )
    }
}


const mapStateToProps =(state)=>({
    errors:state.errors,
    allposts:state.allposts,
    post:state.post
});

export default connect(mapStateToProps,{loadAllPosts})(Home);