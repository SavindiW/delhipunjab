import React, {useState} from "react";
import {useHistory} from "react-router-dom";
import "./AddStyles.css";
import { CloseOutlined } from '@ant-design/icons';
import {loadAllPosts} from '../../actions/auth';
import {connect} from 'react-redux';


function AddPost() {
    const [title,setTitle]=useState("");
    const [description,setDescription] = useState("");
    const [imgURL,setImgURL]=useState("")

    const history = useHistory();
    
    const handlePostUploader = (e)=>{
        e.preventDefault()
        try {
            var myHeaders = new Headers();
            myHeaders.append("x-auth-token", localStorage.getItem('token'));
            myHeaders.append("Content-Type", "application/json");

            var raw = JSON.stringify({"title":title,
                "description":description,
                "imgURL":imgURL});

            var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
            };

            fetch("http://localhost:5000/api/posts/add", requestOptions)
            .then(response => response.json())
            .then(result => {
                if (result.errors){
                    alert(result.errors.msg)
                }
                else{
                    history.push('/')
                }
                
                
            })
            .catch(error => console.log('error', error));
            
        } catch (error) {
            console.log(error);
        }
    }
    
    return (
        <div className="add">
            <div className="add_container container">
                <div className='addpost__close'>
                    <span className='addPost__span'/>
                    <CloseOutlined onClick={()=>history.push('/')}/>
                </div>
                <form action="#" method="POST" name="add form">

                    <div className="header_container">
                        <h1>Add Post</h1>
                    </div>

                    <div className="form_container">
                        <div className="input_container">
                            <output name = "post_title"></output>
                            <input 
                                onChange={e=>setTitle(e.target.value)}
                                value={title}
                                type="text" 
                                name="title" 
                                id="post_title"
                                autoComplete="off"
                                required/>
                            <label for="post_title">Title</label>
                        </div>

                        <div className="input_container">
                            <output name = "description"></output>
                            <input 
                                onChange={e=>setDescription(e.target.value)}
                                value= {description}
                                type="text" 
                                name="description" 
                                id="description"
                                autoComplete="off"
                                required/>
                            <label for="description">Description</label>
                        </div>
                        
                        <div className="input_container">
                            <output name = "imgURL"></output>
                            <input 
                                onChange={e=>setImgURL(e.target.value)}
                                value={imgURL}
                                type="text" 
                                name="imgURL" 
                                id="imgURL"
                                autoComplete="off"
                                required/>
                            <label for="imgURL">Image URL</label>
                        </div>
                        <button 
                            onClick={handlePostUploader}
                            type="submit" 
                            value="Submit">Post Image</button>
                    </div>
                </form>
            </div>
        </div>
    );
}

const mapStateToProps =(state)=>({
    errors:state.errors
});

export default connect(mapStateToProps,{loadAllPosts})(AddPost);
