import React,{useState,useEffect} from "react";
import "./ViewStyles.css";
import Comment from "../../components/comment/Comment";
import { Layout} from 'antd';
import { SendOutlined } from '@ant-design/icons';
import {connect} from 'react-redux';
import Header from "../../components/header/Header";
import Footer from "../../components/footer/Footer";
import { Spin, Space } from 'antd';
import {loadComments} from '../../actions/auth';


export const ViewPost = ({isLoggedIn,user,post,comments,loadComments}) => {
    const { Content } = Layout;
    const [postData,setPostData] = useState([]);
    const [loading,setLoading] = useState(true);
    const [newComment,setComment]=useState("")

    useEffect(()=>{
        if (typeof(post)==="object" && post.length>0){
            setPostData(post);
            setLoading(false)
        }
        
    },[post,comments])

    const handleCommentSend = ()=>{
        if (newComment===""){
            alert("I am not a magician to think what is on your mind. Type the comment please!")
        }
        else{
            var myHeaders = new Headers();
            myHeaders.append("x-auth-token", localStorage.getItem('token'));
            myHeaders.append("Content-Type", "application/json");

            var raw = JSON.stringify({"post_id":post[0].id,"comment":newComment});

            var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
            };

            fetch("http://localhost:5000/api/comments/add", requestOptions)
            .then(response => response.json())
            .then(result => {
                loadComments(post[0].id);
                setComment("");
            })
            .catch(error => console.log('error', error));
        }
    }

    if (loading){
        return(
            <div className='home__loading'>
                <Space size="middle">
                    <Spin size="large" />
                </Space>
            </div> 
        )
    }
    else{
    return (
        <Layout className="layout">
            <Header/>
            <Content className="site-layout post-view" >
                <div className="post_image_view_container">
                    <div className="card">
                        <div className="card_image_container">
                            <img src={postData[0].imgURL} className="card-img-top" alt="..."/>
                        </div>
                        <div className="card-body">
                            <h1 className="card-title">{postData[0].title}</h1>
                            <h4 className="card-author">{postData[0].author}</h4>
                            <h4 className="card-time">{postData[0].created}</h4>
                            <p className="card-text">{postData[0].description}</p>
                            <h3 className="comments_no">{postData[0].comment_count} comments</h3>
                        </div>
                        <div className="comments">
                            {comments.map((commentData,i)=><Comment
                                key={i}
                                author={commentData.author}
                                comment={commentData.comment}
                                />)}                   
                        </div>
                        {isLoggedIn?
                        <div className="user-comments">
                            <p>{user[0]?.username}</p>
                            <input
                                onChange = {e=>setComment(e.target.value)}
                                value={newComment} 
                                type="text" 
                                name="comment" 
                                id="comment" 
                                placeholder="Write a comment..."
                                required
                            />
                            <SendOutlined 
                                onClick={handleCommentSend}
                                style={{position:'relative',marginTop:'-27px'}} />
                        </div> 
                        : null
                        }   
                    </div>                   
                </div>
            </Content>
            <Footer/>        
        </Layout>    
    )}
}

const mapStateToProps =(state)=>({
    isLoggedIn:state.isLoggedIn,
    user:state.user,
    post:state.post,
    comments:state.comments
});

export default connect(mapStateToProps,{loadComments})(ViewPost);