import React, {useState,useEffect} from "react";
import {connect} from 'react-redux';
import {useHistory} from "react-router-dom";
import { CloseOutlined } from '@ant-design/icons';
import "./LoginStyles.css";
import {loginUser} from '../../actions/auth';

function Login({errors,loginUser,isLoggedIn}) {
    const [email,setEmail] = useState("");
    const [password,setPassword] = useState("");
    const history = useHistory();
    const [loginPressed,setLoginPressed]=useState(false);

    useEffect(()=>{
        if (errors){
            if (errors.param && loginPressed){
                alert(errors.msg);
                setEmail("");
                setPassword("");
                setLoginPressed(false)
            }
        }
        if (isLoggedIn){
            setEmail("");
            setPassword("");
            setLoginPressed(false);
            history.push('/');
        }
    },[errors,isLoggedIn])

    const handleLogin = (e)=>{
        e.preventDefault()
        setLoginPressed(true)
        loginUser(email,password);
    }
    return (
        <div className="login">
            <div className="login_container container">
                <div className='addpost__close'>
                    <span className='addPost__span'/>
                    <CloseOutlined onClick={()=>history.push('/')}/>
                </div>
                <form className='false'>
                    <div className="header_container">
                        <h1>Sign In</h1>
                    </div>
                    <div className="form_container">
                        <div className="input_container">
                            <input 
                                onChange={e=>setEmail(e.target.value)}
                                value={email}
                                type="text" 
                                name="email" 
                                id = 'email'
                                autoComplete="off"
                                required/>
                            <label for="email">Email Address</label>
                        </div>

                        <div className="input_container">
                            <input 
                                onChange={e=>setPassword(e.target.value)}
                                value={password}
                                type="password" 
                                name="password"
                                id = 'password'
                                required/>
                            <label for="password">Password</label>
                        </div>
                        <button 
                            onClick={handleLogin}
                            type="submit" 
                            value="Submit">SIGN IN
                        </button>
                    </div> 
                </form>
            </div>
        </div>
    );
}

const mapStateToProps =(state)=>({
    errors:state.errors,
    isLoggedIn:state.isLoggedIn
});

export default connect(mapStateToProps,{loginUser})(Login);