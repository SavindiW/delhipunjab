import React, {useState,useEffect} from "react";
import {useHistory} from "react-router-dom";
import {registerUser} from '../../actions/auth';
import {connect} from 'react-redux';
import { CloseOutlined } from '@ant-design/icons';
import "./RegisterStyles.css";

function Register({errors,isLoggedIn,registerUser}) {
    const history = useHistory();
    const [username,setusername] = useState("");
    const [email,setEmail] = useState("");
    const [password,setPassword]= useState("");
    const [rePassword,setRePassword]= useState("");
    const [registerPressed,setRegisterpressed]=useState(false);

    useEffect(()=>{
        if (errors){
            if (errors.param && registerPressed){
                alert(errors.msg);
                setEmail("");
                setPassword("");
                setusername("");
                setRePassword("")
                setRegisterpressed(false)
            }
        }
        if (isLoggedIn){
            setEmail("");
            setPassword("");
            setRegisterpressed(false);
            history.push('/');
        }
    },[errors,isLoggedIn])

    const handleSignUP = (e)=>{
        e.preventDefault();
        setRegisterpressed(true);
        if (password === rePassword){
            registerUser(username,email,password)
        }
        else{
            alert("Passwords do not mach each other!")
        }

    }

    return (
        <div className="register">
            <div className="register_container container">
                <div className='addpost__close'>
                    <span className='addPost__span'/>
                    <CloseOutlined onClick={()=>history.push('/')}/>
                </div>
                <form className='false' name="register form">
                    <div className="header_container">
                        <h1>Sign Up</h1>
                    </div>
                    <div className="form_container">
                        <div className="input_container">
                            <input 
                                onChange={e=>setusername(e.target.value)}
                                value={username}
                                type="text" 
                                name="uname" 
                                id="uname"
                                autoComplete="off"
                                required/>
                            <label for="uname">Username</label>
                        </div>
                        <div className="input_container">
                            <input 
                                onChange = {e=>setEmail(e.target.value)}
                                value={email}
                                type="text" 
                                name="email" 
                                id="email" 
                                autoComplete="off"
                                required/>
                            <label for="email">Email</label>
                        </div>
                        <div className="input_container">
                            <input
                                onChange = {e=>setPassword(e.target.value)}
                                value={password} 
                                type="password" 
                                name="psw" 
                                id="psw" 
                                required/>
                            <label for="psw">Password</label>
                        </div>
                        <div className="input_container">
                            <input 
                                onChange={e=>setRePassword(e.target.value)}
                                value={rePassword}
                                type="password" 
                                name="psw-repeat" 
                                id="psw-repeat" 
                                required/>
                            <label for="psw-repeat">Confirm Password</label>
                        </div>
                        <div className="checkboxes margin">
                            <label for="cb1">I am 13+</label>
                            <input type="checkbox" id="cb1"/>
                            <label for="cb1" className="recheck tick_con1">
                                <div className="tick_con">
                                    <div className="tick cb1"></div>
                                </div>
                            </label>
                        </div>
                        <a href="#">TOS and privacy</a>
                        <div className="checkboxes">
                            <label for="cb2" className="condition">TOS and privacy rules</label>
                            <input type="checkbox" name="TOS" value="TOS" id="cb2"/>
                            <label for="cb2" className="recheck tick_con1">
                                <div className="tick_con">
                                    <div className="tick cb2"></div>
                                </div>
                            </label>
                        </div>
                        <button 
                            onClick={handleSignUP}
                            type="submit" value="Submit">SIGN UP
                        </button>
                    </div>
                </form>
            </div>
        </div>
    );
}

 const mapStateToProps =(state)=>({
    errors:state.errors,
    isLoggedIn:state.isLoggedIn
}); 

export default connect(mapStateToProps,{registerUser})(Register);