import React,{useEffect} from 'react';
import './App.css';
import {Provider} from 'react-redux';
import store from './Store';
import {loadUser} from './actions/auth';
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";
import Login from './pages/login/Login';
import Register from './pages/register/Register';
import Home from './pages/home/Home';
import AddPost from './pages/addPost/AddPost';
import ViewPost from './pages/viewPost/ViewPost';

function App() {
  useEffect(()=>{
    store.dispatch(loadUser());
  },[]);

  return (
    <Provider store={store}>
      <Router>
        <Switch>          
          <Route exact path="/" component={Home}/>
          <Route exact path="/register" component={Register}/>
          <Route exact path="/login" component={Login}/>
          <Route exact path="/addpost" component={AddPost}/>
          <Route exact path="/viewpost" component={ViewPost}/> 
        </Switch>         
      </Router>
    </Provider>
    
  );
}

export default App;
