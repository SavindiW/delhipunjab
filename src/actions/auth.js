import {REGISTER_SUCCESS,
    LOGIN_SUCCESS,
    REGISTER_FAIL,
    LOGIN_FAIL,
    LOAD_USER,
    AUTH_ERROR,
    SIGN_OUT,
    LOAD_POST,
    LOAD_POST_FAIL,
    LOAD_ALL_POSTS,
    LOAD_ALL_POSTS_FAIL,
    LOAD_COMMENTS,
    LOAD_COMMENTS_FAIL } from '../constants/constants';

export const loadUser =()=>async dispatch => {
    if (localStorage.getItem('token')){
        var myHeaders = new Headers();
        myHeaders.append("x-auth-token", localStorage.getItem('token'));
        myHeaders.append("Content-Type", "application/json");
        var requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
        };

        fetch("http://localhost:5000/api/user/load", requestOptions)
        .then(responce =>responce.json())
        .then((data)=>{
            dispatch({
                type:LOAD_USER,
                payload:data
                })
        })
        .catch(error => {
            dispatch({
                type:AUTH_ERROR,
                payload:error
            })});
    }
    else{dispatch({
        type:AUTH_ERROR,
        payload:"Local Storage donot have a token"
    })}
};
 
export const registerUser = (username,email,password)=>async dispatch =>{
    try {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        var raw = JSON.stringify({username:username,email:email,password:password});
        var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: raw,
        redirect: 'follow'
        };

        fetch("http://localhost:5000/api/user/register", requestOptions)
        .then((response=>response.json()))
        .then((result)=>{
            if (result.errors){
                dispatch({
                    type:REGISTER_FAIL,
                    payload:result.errors
                })
            }
            else{
                if (result.token){
                    dispatch({
                        type:REGISTER_SUCCESS,
                        payload:result
                    });
                    dispatch(loadUser())
                }
            }
            
        })
        .catch(error => dispatch({
            type:REGISTER_FAIL,
            payload:error
        }));
    } catch (error) {dispatch({
        type:REGISTER_FAIL,
        payload:error
    })
    }
};

export const loginUser = (email,password)=> async dispatch =>{
    try {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var raw = JSON.stringify({email:email,password:password});

        var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: raw,
        redirect: 'follow'
        };

        fetch("http://localhost:5000/api/user/login", requestOptions)
        .then(response => response.json())
        .then((result)=>{
            if (result.errors){
                dispatch({
                    type:LOGIN_FAIL,
                    payload:result.errors
                })
            }
            else{
                if (result.token){
                    dispatch({
                        type:LOGIN_SUCCESS,
                        payload:result
                    }); 
                    dispatch(loadUser())
                }
            }
        })
        .catch(error => dispatch({
            type:LOGIN_FAIL,
            payload:error
        }));
    } catch (error) {dispatch({
        type:LOGIN_FAIL,
        payload:error
    })
    }
};

export const signout = () => dispatch=>{
    dispatch({
        type:SIGN_OUT,
        payload:""
    }); 
};

export const loadPost = (id) =>async dispatch=>{
    try {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        var raw = JSON.stringify({"post_id":id});

        var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: raw,
        redirect: 'follow'
        };

        fetch("http://localhost:5000/api/posts/readOne", requestOptions)
        .then(response => response.json())
        .then(result => {
            dispatch({
                type:LOAD_POST,
                payload:result
            }); 
        })
        .catch(error => {
            console.log('error', error);
            dispatch({
                type:LOAD_POST_FAIL,
                payload:error
            }); 
        });
    } catch (error) {
        console.log(error)
        dispatch({
            type:LOAD_POST_FAIL,
            payload:error.message
        });
    }
}

export const loadAllPosts = () =>async dispatch=>{
    try {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
        };

        fetch("http://localhost:5000/api/posts/readAll", requestOptions)
        .then(response => response.json())
        .then(result => {
            dispatch({
                type:LOAD_ALL_POSTS,
                payload:result
            }); 
        })
        .catch(error => {
            console.log('error', error);
            dispatch({
                type:LOAD_ALL_POSTS_FAIL,
                payload:error
            }); 
        });
    } catch (error) {
        console.log(error)
        dispatch({
            type:LOAD_ALL_POSTS_FAIL,
            payload:error.message
        });
    }
}

export const loadComments = (id) =>async dispatch=>{
    try {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var raw = JSON.stringify({"post_id":id});

        var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: raw,
        redirect: 'follow'
        };

        fetch("http://localhost:5000/api/comments/read", requestOptions)
        .then(response => response.json())
        .then(result => {
            dispatch({
                type:LOAD_COMMENTS,
                payload:result
            }); 
        })
        .catch(error => {
            console.log('error', error);
            dispatch({
                type:LOAD_COMMENTS_FAIL,
                payload:error
            }); 
        });
    } catch (error) {
        console.log(error)
        dispatch({
            type:LOAD_COMMENTS_FAIL,
            payload:error.message
        });
    }
}