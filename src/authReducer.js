import {REGISTER_SUCCESS,
    LOGIN_SUCCESS,
    REGISTER_FAIL,
    LOGIN_FAIL,
    LOAD_USER,
    AUTH_ERROR,
    SIGN_OUT,
    LOAD_POST,
    LOAD_POST_FAIL,
    LOAD_ALL_POSTS,
    LOAD_ALL_POSTS_FAIL,
    LOAD_COMMENTS,
    LOAD_COMMENTS_FAIL } from './constants/constants';

const initialState={
    token:localStorage.getItem('token'),
    isLoggedIn:false,
    user:{},
    errors:{},
    allposts:{},
    post:{},
    comments:[]
};

const authReducer = (state=initialState,action)=>{
    const {type,payload} = action;
    switch(type){
        case REGISTER_SUCCESS:{
            return{
                ...state,
                isLoggedIn:true,
                errors:{}
            }
        }
        case LOGIN_SUCCESS:
            localStorage.setItem('token',payload.token);
            return {
                ...state,
                isLoggedIn:true,
                errors:{},
                user:{}
            };
        case LOAD_USER:{
            localStorage.getItem('token');
            return {
                ...state,
                isLoggedIn:true,
                user:payload,
                errors:{}

            }
        };
        case LOGIN_FAIL:
        case AUTH_ERROR:
            localStorage.removeItem('token');
            return {
                ...state,
                isLoggedIn:false,
                errors:payload,
                user:{}
            };
        case SIGN_OUT :
            localStorage.removeItem('token');
            return {
                ...state,
                isLoggedIn:false,
                errors:payload,
                user:{}
            };
        case REGISTER_FAIL:
            return{
                ...state,
                errors:payload
            };
        case LOAD_POST:
            return{
                ...state,
                post:payload
            }
        case LOAD_POST_FAIL:
            return{
                ...state,
                post:{},
                errors:payload
            }
        case LOAD_ALL_POSTS:
            return{
                ...state,
                allposts:payload
            }
        case LOAD_ALL_POSTS_FAIL:
            return{
                ...state,
                allposts:{},
                errors:payload
            }
        case LOAD_COMMENTS:
            return{
                ...state,
                comments:payload
            }
        case LOAD_COMMENTS_FAIL:
            return{
                ...state,
                comments:[],
                errors:payload
            }
  
        default:
            return state
    }
};

export default authReducer;