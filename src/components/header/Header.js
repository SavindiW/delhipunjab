import React, {useState} from "react";
import { Layout, Button, Input, Collapse} from "antd";
import { PlusSquareOutlined, MenuOutlined, LogoutOutlined,UserOutlined,SearchOutlined } from '@ant-design/icons';
import {useHistory} from "react-router-dom";
import {connect} from 'react-redux';
import {signout} from '../../actions/auth';
import "./HeaderStyles.css";

export const MainHeader = ({isLoggedIn,user,signout}) => {

    const {Header} = Layout;
    const { Search } = Input;
    const { Panel } = Collapse;
    const history = useHistory();
    const [collapsed,setCollapsed] = useState(false);

    function addPost() {
        history.push("/addpost");
    }

    function signIn() {
        history.push("/login");
    }

    function signUp() {
        history.push("/register");
    }

    function signOut() {
        signout();
        history.push("/login");
    }

    function homePage() {
        history.push("/");
    }

    function collapse() {
        if(collapsed){
            document.getElementById("header").style.position = "sticky";

        }else{
            setCollapsed(true);
            document.getElementById("header").style.position = "fixed";
        }        
    }


    return (
        <Collapse 
            defaultActiveKey={['1']} 
            bordered={false}
            expandIcon={({ isActive }) => <MenuOutlined rotate={isActive ? 90 : 0} className="menu-icon"/>}
            className="site-collapse-custom-collapse"
            onChange={collapse}
        >
            <Panel 
                key="1"    
            >
                <Header className="header" id="header">
                    <div className="logo">
                        <Button type="link" danger onClick={homePage}>
                            <h3>
                                POST<span>Viewer</span>
                            </h3>
                        </Button>
                        
                    </div>
                    <div className="search-bar">
                        <Input
                        placeholder="search..."
                        style={{ margin: '0 10px' }}
                        suffix={<SearchOutlined />}
                        />
                    </div>
                    
                    {isLoggedIn?
                        <div className="user-button">
                            <span className="header-username">
                                <UserOutlined style={{marginRight:'10px'}}/>{user[0]?.username}
                            </span>
                            <Button type="link" shape="round" icon={<PlusSquareOutlined/>} danger onClick={addPost} className="button-newpost"> 
                                New Post
                            </Button>
                            <Button type="link" danger shape="round" onClick={signOut} className="button-signout">
                                <LogoutOutlined />
                            </Button>
                        </div>
                        :
                        <div className="user-button">
                            <Button type="link" shape="round" danger className="button-sign" onClick={signIn}>
                                Sign In
                            </Button>
                            <Button type="link" shape="round" className="button-sign" danger onClick={signUp}> 
                                Sign Up
                            </Button>
                        </div>
                    }    
                </Header>
            </Panel>
        </Collapse>      
    )
}

const mapStateToProps =(state)=>({
    isLoggedIn:state.isLoggedIn,
    user:state.user
});

export default connect(mapStateToProps,{signout})(MainHeader);