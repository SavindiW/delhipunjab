import React,{useState} from "react";
import { Card, Button } from 'antd';
import { MessageOutlined, EyeOutlined, LikeOutlined } from '@ant-design/icons';
import "./CardStyles.css";
import {loadPost,loadComments} from '../../actions/auth';
import {connect} from 'react-redux';
import {useHistory} from 'react-router-dom'

export const PostCard = ({id,title,description,imgURL,likes,views,comment_count,loadPost,loadComments}) => {
    const [likeCount,setLikeCount] = useState(likes);
    const { Meta } = Card;
    const history = useHistory();

    const handleLike =()=>{
        try {
            var myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/json");

            var raw = JSON.stringify({"post_id":id});

            var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
            };

            fetch("http://localhost:5000/api/posts/like", requestOptions)
            .then(response => response.json())
            .then(result => {
                if (!result.errors){
                    setLikeCount(likeCount+1)
                }
            })
            .catch(error => console.log('error', error));
        } catch (error) {
            console.log(error)
        }
    }

    const handlePostView = ()=>{
        loadPost(id);
        loadComments(id);
        history.push('/viewpost')
    }

    return ( 
        <Card
            className='card__container'
            style={{ width: 250, marginLeft:'auto', marginRight:'auto' }}
            bordered={false}
            cover={
                <img
                alt="example"
                src={imgURL}
                onClick={handlePostView}
                />
            }
            actions={[
                <Button 
                    onClick={handleLike}
                    type="link" 
                    icon={<LikeOutlined />} 
                    className="card-icon">{likeCount}</Button>,
                <Button 
                    onClick={handlePostView}
                    type="link" 
                    icon={<MessageOutlined />} 
                    className="card-icon">{comment_count}</Button>,
                <Button 
                    type="link" 
                    icon={<EyeOutlined />} 
                    className="card-icon">{views}</Button>,
            ]}
            hoverable
        >
            <Meta
                title={title}
                description={description}
                onClick={handlePostView}
            />
        </Card>
    )
}

const mapStateToProps =(state)=>({
    errors:state.errors
});

export default connect(mapStateToProps,{loadPost,loadComments})(PostCard);