import React from "react";
import {Layout} from 'antd';
import "./FooterStyles.css";

export const MainFooter = () => {

    const {Footer} = Layout;
    
    return ( 
        <Footer className="footer-line">
             All rights reserved.
        </Footer> 
    )
}

export default MainFooter;