import React from "react";
import { Card} from 'antd';
import "./CommentStyles.css";

export const Comment = ({author,comment}) => {
    const { Meta } = Card;
    return ( 
        <Card 
            style={{  marginTop: 16 , borderBottom: '1px solid grey'}}
            bordered={false}
            bodyStyle={{ background:'#262A3A'}}
        >
            <Meta
            
                title={author}
                description={comment}
            />
        </Card>
    )
}

export default Comment;